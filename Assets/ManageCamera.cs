﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageCamera : MonoBehaviour {

	public Camera alleyCamera, streetCamera,alleyCamera2;
	public bool alley,alley2= false;
	bool wasInAlley2=false;
	Transform cameraTransform;
	public float rotationTime = 0.5f;
	public float rotationValue = 0.01f;

	public Color fogBright, fogDark;

	// Use this for initialization
	void Start () {
		cameraTransform = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (alley&&!alley2) {
			if (rotationValue < rotationTime) {
				Debug.Log ("alley in");
				rotationValue += Time.deltaTime;

			}
			cameraTransform.position = Vector3.Slerp (streetCamera.transform.position, alleyCamera.transform.position, rotationValue / rotationTime);
			cameraTransform.rotation = Quaternion.Lerp (streetCamera.transform.rotation, alleyCamera.transform.rotation, rotationValue / rotationTime);
			Camera.main.farClipPlane = Mathf.Lerp (streetCamera.farClipPlane, alleyCamera.farClipPlane, rotationValue / rotationTime);
			Camera.main.nearClipPlane = Mathf.Lerp (streetCamera.nearClipPlane, alleyCamera.nearClipPlane, rotationValue / rotationTime);
			Camera.main.fieldOfView = Mathf.Lerp (streetCamera.fieldOfView, alleyCamera.fieldOfView, rotationValue / rotationTime);
			Camera.main.orthographicSize = Mathf.Lerp (streetCamera.orthographicSize, alleyCamera.orthographicSize, rotationValue / rotationTime);
			RenderSettings.fogColor = Color.Lerp (fogDark, fogBright, rotationValue / rotationTime);


		} 
		if(!alley&&!alley2&&!wasInAlley2){
			if (rotationValue > 0) {
				Debug.Log ("alley out");
				rotationValue -= Time.deltaTime;

			}
			cameraTransform.position = Vector3.Slerp (streetCamera.transform.position, alleyCamera.transform.position, rotationValue / rotationTime);
			cameraTransform.rotation = Quaternion.Lerp (streetCamera.transform.rotation, alleyCamera.transform.rotation, rotationValue / rotationTime);
			Camera.main.farClipPlane = Mathf.Lerp (streetCamera.farClipPlane, alleyCamera.farClipPlane, rotationValue / rotationTime);
			Camera.main.nearClipPlane = Mathf.Lerp (streetCamera.nearClipPlane, alleyCamera.nearClipPlane, rotationValue / rotationTime);
			Camera.main.fieldOfView = Mathf.Lerp (streetCamera.fieldOfView, alleyCamera.fieldOfView, rotationValue / rotationTime);
			Camera.main.orthographicSize = Mathf.Lerp (streetCamera.orthographicSize, alleyCamera.orthographicSize, rotationValue / rotationTime);
			RenderSettings.fogColor = Color.Lerp (fogDark, fogBright, rotationValue / rotationTime);
		}

		if (alley2 && !alley) {
			if (rotationValue < rotationTime) {
				Debug.Log ("alley2 in");
				rotationValue += Time.deltaTime;

			}
			cameraTransform.position = Vector3.Slerp (streetCamera.transform.position, alleyCamera2.transform.position, rotationValue / rotationTime);
			cameraTransform.rotation = Quaternion.Lerp (streetCamera.transform.rotation, alleyCamera2.transform.rotation, rotationValue / rotationTime);
			Camera.main.farClipPlane = Mathf.Lerp (streetCamera.farClipPlane, alleyCamera2.farClipPlane, rotationValue / rotationTime);
			Camera.main.nearClipPlane = Mathf.Lerp (streetCamera.nearClipPlane, alleyCamera2.nearClipPlane, rotationValue / rotationTime);
			Camera.main.fieldOfView = Mathf.Lerp (streetCamera.fieldOfView, alleyCamera2.fieldOfView, rotationValue / rotationTime);
			Camera.main.orthographicSize = Mathf.Lerp (streetCamera.orthographicSize, alleyCamera2.orthographicSize, rotationValue / rotationTime);
			RenderSettings.fogColor = Color.Lerp (fogDark, fogBright, rotationValue / rotationTime);
			wasInAlley2 = true;

		} 
			

		if(!alley&&!alley2&&wasInAlley2){
			if (rotationValue > 0) {
				Debug.Log ("alley2 out");
				rotationValue -= Time.deltaTime;
				cameraTransform.position = Vector3.Slerp (streetCamera.transform.position, alleyCamera2.transform.position, rotationValue / rotationTime);
				cameraTransform.rotation = Quaternion.Lerp (streetCamera.transform.rotation, alleyCamera2.transform.rotation, rotationValue / rotationTime);
				Camera.main.farClipPlane = Mathf.Lerp (streetCamera.farClipPlane, alleyCamera2.farClipPlane, rotationValue / rotationTime);
				Camera.main.nearClipPlane = Mathf.Lerp (streetCamera.nearClipPlane, alleyCamera2.nearClipPlane, rotationValue / rotationTime);
				Camera.main.fieldOfView = Mathf.Lerp (streetCamera.fieldOfView, alleyCamera2.fieldOfView, rotationValue / rotationTime);
				Camera.main.orthographicSize = Mathf.Lerp (streetCamera.orthographicSize, alleyCamera2.orthographicSize, rotationValue / rotationTime);
				RenderSettings.fogColor = Color.Lerp (fogDark, fogBright, rotationValue / rotationTime);
			}


		}
		if (rotationValue <= 0) {
			wasInAlley2 = false;
		}

	}
}
