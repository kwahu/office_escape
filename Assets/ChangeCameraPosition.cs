﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraPosition : MonoBehaviour {
	private ManageCamera cameraManager;
	public bool alley2;
	// Use this for initialization
	void Start () {
		cameraManager = FindObjectOfType<ManageCamera> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter(Collider other){
		if (alley2) {
			cameraManager.alley2 = true;
		} else {
			cameraManager.alley = true;
		}

	}
	void OnTriggerExit(Collider other){
		if (alley2) {
			cameraManager.alley2 = false;
		} else {
			cameraManager.alley = false;
		}
	}
}
